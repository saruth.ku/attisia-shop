@extends('templates.templates_dashboard')
@section('css')
<!-- ================== เขียนเอง core-css ================== -->
<link href="{{ asset('assets/css/default/handlecss/productCss/handle.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/css/default/handlecss/productCss/handleModalPreviewImg.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/css/default/handlecss/productCss/handleInputUpload.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/library_plugins/datetime_picker_full/jquery.datetimepicker.min.css') }}" rel="stylesheet" />

<!-- ================== เขียนเอง core-css ================== -->
<!-- ================== dropzone page-css ================== -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.9.3/dropzone.css" />
<!-- ================== dropzone page-css ================== -->
@endsection
@section('content')

<div id="content" class="app-content">
    <ol class="breadcrumb float-xl-end">
        <li class="breadcrumb-item"><a href="javascript:;">บัญชีธนาคาร</a></li>
        <li class="breadcrumb-item"><a href="javascript:;">บัญชีธนาคารทั้งหมด</a></li>
        <li class="breadcrumb-item active">เพิ่มบัญชีธนาคาร</li>
    </ol>
    <h1 class="page-header"><small>เพิ่มบัญชีธนาคาร...</small></h1>
</div>

<div id="content" class="app-content">
    <div class="row">
        <div class="col-xl-2"></div>
        <div class="col-xl-8 ui-sortable">
            <div class="panel panel-inverse" data-sortable-id="form-validation-1">
                <div class="panel-heading ui-sortable-handle">
                    <h4 class="panel-title">ช่องทางโอนเงินบัญชีธนาคาร</h4>
                </div>

                
                <div class="panel-body">
                <form id="form-add-bank" method="POST" action="{{ route('listbank2') }}"
                            data-parsley-validate="true" enctype="multipart/form-data">
                            @csrf
                    <div class="form-group row mb-3">
                        <label class="col-lg-3 col-form-label form-label">ธนาคาร <span class="text-danger">*</span> :</label>
                        <div class="col-lg-5 pt-2">
                            <div class="card card-body">
                                <div class="form-check">
                                    <input type="radio" class="form-check-input" name="shop_bank"  id="SCB1" value="ธนาคารไทยพาณิชย์" data-parsley-required="true" data-parsley-multiple="radiorequired">
                                    <label class="form-check-label h5" for="SCB1"><img width="20" height="20" 
                                    src="https://play-lh.googleusercontent.com/j-9a3HbVZoX337-MLdkmYt75yUfN5ahis8rOnE09972cFLdVn7Z5Dzu3Guo8ldUv2H4x" alt=""> <span class="badge bg-indigo ms-1">ธนาคารไทยพาณิชย์</span></label>
                                </div>
                                <div class="form-check mt-3">
                                    <input type="radio" class="form-check-input" name="shop_bank" id="KBANK2" value="ธนาคารกสิกรไทย" data-parsley-multiple="radiorequired">
                                    <label class="form-check-label h5" for="KBANK2"><img width="20" height="20" 
                                    src="https://www.kasikornbank.com/SiteCollectionDocuments/about/img/logo/logo.png" alt=""> <span class="badge bg-green ms-1">ธนาคารกสิกรไทย</span> </label>
                                </div>
                                <div class="form-check mt-3">
                                    <input type="radio" class="form-check-input" name="shop_bank" id="BBL3" value="ธนาคารกรุงเทพ" data-parsley-multiple="radiorequired">
                                    <label class="form-check-label h5" for="BBL3"><img width="20" height="20" 
                                    src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAmVBMVEUBMZT///8AL5MALZMAKJEAG44AKpIAEowAJZAAI5AAIo8ADIsAK5IAGI0AH48AFIzm6vP5+v3z9frZ3uzGzeKGlMKzvNje4+/R1+j19/vJ0OQgRJ0VPZoKNpe6wtxidbJbb6+WosoyTqB+jL1uf7c+V6R2hrunsdJVaq2RncdLYqqrtdS0vdmcqM1LYqksSp4AAIoaQp0LO5qtqqjiAAARq0lEQVR4nM1de1uqTBCHXW6KgJqmaV7yUmmZvX3/D/ciXndnQJgd6swf53lOpfCD2blfLLt2emqPprvNcrVd7MfPE2vyPN4vtqvlZvc5aj/Vf3mrzi9vj3av65/Yd+Nm4DhSCiEsy0r/ldJxgmbs+vHP+nU3atd5E3UhbE/729iP4kBmqPIoBRvEkR9v+9O6YNaBsP2xenbdRqsImga01XCjn9VjHSi5ET71lvswdmRpcFeSThzuX3sd5jtiRdiZzuIooKC7onTj2ZQVJCPC6dx1vfKcmUciA8l3W1wIh68xB7wzyMjpD5jujAfhx9gPuOCdQDb89QfLvTEgbG/i2OTs5ZGM4033H0A4WLnNGuAdqemujJnVEOFgHjq87KmScMK5IUYjhA/zxKkR3pGcxAyjAcL2PPRqx3cgL5wbGDtkhJ3lL7y/MznJkmwFUBG+Bc06z59Ooum8/SrCh7H7m/gyjO744fcQLv069N89kv7ylxD2ZH0KsJiaovcbCFd/8gKPJP1V7QiHk8af4TtQYzKsF+HmD1/gkaS/qRFhd/HrIhSScBeV7PEqCEfB79gw98gLRvUg3IR//wKPJMKXOhB+uX8N7Iaid3aE3X3w16gUauzLHsaSCB/+kSN4JS8oacSVQziN/lpJQJJJOQOnFMK3f0bGKBSWcjfKINyFf40FJxHueBC++Jx3xWoz+C8cCDesAIMha2C1hAl3FyErQCvs2T1Wnr8P8R5CVha14n76lf2Y8yvvMuodhG+sAJ119qVr1hCWf0eiFiP8YOUoKY92SFeyatewOL9RiHDEqwe/z87r8Jvza0VY6GoUIRxErACjl8s3v0ScXyzcoqB4AcInXmYKbt2Bd1Y7XsqCqpUChHtWY1tat1HrjsX69Lw9BeE7a8hJJKor8JCwnoBGvr+Yi5D3qECZzquHbg95SYS8lofVmIErrHjDkmGeL5WDsNtgZSL5H3KN/1iPomjkOP05CBesUka4WP6vzRua9BZVEPKajpb/iF7lkfcoZkZvSYQj5ku/4k/XfmV+kKhtgyHkVlY57GNzHwY5KYtwxmpwCDc/7tflPYoNLDOFIJzy8qhfFBLrMV8LqYdDEPIWyLj4+T9TnzWSLrwyCF9ZNbGzLQRo21tWd7gBhRpAOGTlG+noh1APAHZJ1ba55IMEKkDIammIUL/gy7cOccgbRwDWk47wjfVcAHs4dSkSPd/Aa+O7uomvIeywipkA+DQTackf/Yes7rBwtOopDeGSs5BETvRSrcyfAFqrM+E8GU1NeKsI25x+qQh1fjwZov6n9vMHzoiXSFQzX0U45xTdiX4i2v4RiPB1V+MtYbyuM89H+MAp1qDTu2idfuWt9V/NOJWwyjsKwndGQxiK7eXVk4CODqeS8hQBd4vwgZFXRKRzohIXAUFc1thscquFbxFyvsJQt4G7SlZNOHqEc8p4QpSXeIOQ8xRCp/dLFWIOUJWc7nB4EwS/QTjjE6RQlADDJQIZakZ32LmRcleEjIEhGPeCGk/4urZs88X3bqXAFeGGz5wBTm/nB4pK+ay/REZ3uHEtJ74i5HuF0OlF1R2MOfC5wyKBCD/YDjp0ej/wlwOsN0Z3OL6kTS8IF1wqVwJFkHfCYaD4ic0dlpcA3xnhA9sZgBnZfSvnT1sgzjhi01gXQXZG+Mrlo8Ek0DKf/2PQX8DmDgdnjXxGyCVIgy/9ngslJAw1fnE96qaKcMokxaDTW5zFEoGuOdncYXeqIJwz2RMg8qRbazpB640rMuXNbxE+MSlDaIrdPVfwIzueoyjcpxuEnzxMCiNPQ//eo4PWG1dk6sSmFiOTSqkfQsxaA58CsbcOT5nLiU0zhJ2Yg0kFjDeXCk5A6+3+my91P3HngrDHwvlQEz6WCxokIEW8YTk1Ue+CcMnB+PAQtksGCWHsjcdADZYXhHsGvpce6NRd5FlrOkHrrctxbuT+jLDNoYFgEr3AWtMJWm8svmLYPiHkcJxgfLBSzRGs96nwfPJv6uOEkCFA0wKlc91KOR7hgWT/2PzoZOGaA8ISSuveDcamssIBFvvA3M7KAiUpwra5rvBBIfJLVXkPrbc38/s6BKQsDr8imOt3N6ycTcKsN+PTczDcUoR9U20IA9gUxoexN/PywaCfIVybHkMoCEmpJJisMi4BlesMoWlOBNqVObG1ewQL/ExrUEV0QDgw1K0SlHO0ic8Mxt6MK+z8QYqwZyhoYKglN7Z2j6D1ZmrauL0U4YtZEAryqIE1Ai0jwyrC5kuK0MyiAcUdRsWp0MV8MhuDk1o1lmGwG1YDPhtNFQTW34dRYlouUoQTk4fkAafw0cwSScATW1NP9YHExLbMili/gT1qdEPYIxuYtIEJt2sNTLggBh2cbUPhJyLgZCxNlGIysEyUBVJYbWzkRkD5GNXauT3r0cDTjGCXgaHuSdkCtoSa1EvGj9aOzgNQ8KVmvGlxUwNpXTZwYJs7y8CzwMY2bEwRNhGEn3T5HPStJVnhS1BScuAo0/BKjHX10oOBztJakQP6aJvByNQxj7DGlyn5W72VtaU+Hom2wphmsRBtcSByYZ/cWmSjzQWVFBkZavwWxvoGzC8X1p74zPEmI+ODiCiLA3Wo0zTE3hoTbwWTedm9GBVSizBniic1tSLG1jPxhmA25UQbk5eol6FfiBqJEM/WhPZJaCFfXqLBWG/Ryh3ESo2XEfEdrKG8WzGphYV1YBfakVmDhlFEysN+UgKmK6pt2lTiiU/KJdpEH2hCPIeemmZYqf8l1sI6qob9UuuMaWotPYc0WaoK9SdXTVx0SUFAOVE44S2JlZdItHjHRH3oK9NEPmLNFHmglBokynd2faHaFEOS5ZbqQ9LLF6q637YsTy0qJcQ5tdzA1rtUNR2pQ8pipDYNyS5VG2+yUI+WHXupKhgS9fOH1JpoKmxK0hepXTqjSIXmy+2ls8iFCFUTYFVNumvtC8cyjkiJnpLMGm9G8w/dKby0HpKvlAQOtMrpYxmH+iBJ5Qapf0jy8VWhcGIfLfBWpYxSN+NPFUPqYSCJmtTHJ8VpVPs4OjfdqVnc8v0TIlA5/CyKVYFGClQ2d6RYm4jRK+vtaqPvkjVR31q24hJ5ChUNSbEG40dSvFQNsl25p6kV/pRsnNTrHK550UjhCgq3uT1SzLulGGnTKxfooZtlmcfnas/lZmqFq3wfJZSRDEh5C0fJGd549SLQahZKjNNqaIUctzekejAEhXjIW1ByTyrC2zA3KNse31O3YBDZ9uYTqvlL6I885J4oNns+QlBkem8oxHmW4oWU0lJVIa6qq+4sf0jIATuKAaIa/XqLerERDuqE1Ep9FeEr4U5ntDy+ilD9AlD481kk5PVmWq0w3PgdZnl8grpQuXSnPiJQ+LPJt0VA4fRctbCaijlOQJjVYhCiWCpC3V5M9MzDPI9LmvrD0BWomsQgIMzqaezqAU7VXtSZANbgrXEh6OjxbXBmVX1YXZYea6IIakbNOgGLGHRQPLWwS4CZnLACSmmsJ0j9U11bdb9LhdAFogRUY2LVsLDqFvpbqoVfXXOfahMJqXffLr4ykCBIlSGoaIRdwJpcrm55n+pLCTXCoaKmkZ4iULm/081fMPkECSQ7iiTqVheJpxphwttXJyEh6SbIgtrIBDB0AXvOqiitPrYj8y9ptfqxEufDwtGweH97e9xhqylWmq/O0qkexbjU6lf/aENNEWHSGMy97Txfc6etsQ4Q68hrqcGb6hLx0m9RvWdGu/YHJqvA3IH2pdNHNHQeRh3JSM3TVNdql56Z6uWEIlZVGRp0AqODzwX8cIobWjatqdXqjuxRFGcIq7eqq+FEe4e9AgH0wWk6MhgTP0V7F7TJa9WV2rFhndh/6GiVwWjVkkj0WemZyot09sUdLD2uVV0e3vQfVu8hFa6arMX7BqSlN2K8N6AIylkkoM1m6FQujz8lBSzi89FrJvAJQXD87D6GP0INar1hr3qNx8leOCKszuNglBeeFYUvDGyiwocogPBN9UCb0stNaHXWKy9zAjK5U5LPNEPfjRTag0AVUiGdGp3PMxWqsyloJMkJyCTFM2jxiKqMNBlFKPA4G7X0uRig9GWYoG8Rdu7d0A61pmWkK0xCJbQ2F8MmVFUBvT10UYjf+StEpmiVequhBwkIszJEw1YREtKPcBhCe4IxOz5z/kD4CojgB7Q/EeadBGdZfEZIqaqCHfidLcbt8gevdMKFU/QO/prSnX+ppTCaE4VMskZXsSK7H/JuXCLrKSnT+K6hJLNZX8g6qaGF8DuYonggbEpLIOAy3A2lGgqZ9WWT6saQBX2dOTxd6H4L2EYhkhnkZ9IyE+FePm86c89HFvTtoNqAY0+Q2m3PRRQLbS3ajYtuPDcRW+02+E9XX0jpL5De8QKRuX0SQHxuIiVofqAEs8tmmscXQJmkRehkiBk/M1qRPj770h4Q60LjL0QZTGMFgO5P2nqMPmghdkF3TazjzJlfSp5BGzwjW8Hai1vR5elRcPUdiuQd2bo1lMRul7wZtPQ5wvIbsz37N6rRgRrxptxMRliF/ts3tVFCCefyzIIWCbYdZCQuwgTh0uuxb2I8YM/IjZFO7ixok0nCjWdkAfFVNQZQHJ3DXyLERNXDhN4iVjDP22Qmu8Q0o/12Uo0BGCJ01hZejNk7L5jxV5KKZrIbzdUX0RfSsjQYZ+IQ6SrMOhWFu0aUYHdrMMmieK6+3TdpAXUCrJvt9eD7qyUHx9fUPLx4rPOmF5iMCCjejWC430L4kBlt+zPxMJtm17QCDxpzKfsazaO7t9/CdEdJvMdU49rFuntjzM62B3uz1rB7O0qMB1C1Ikw1bhCF+fGNKkE82FOaJMhqAYSmA4WFP0feTBv7GfxR5910hQicxVTDvqdggp2uMjTCvOdKVGbfk20b9J4dSYb394Fj1A9N5zmJFvzWevauuev8bXJ5lMoj4+uW3LvGsRIFt1SK6NM1nxKLRrzQDY8M+w+Fj9ni+bRiGMoKJ1blImTZYdkcF+2SVunhP4554uV3WDLtIZVIIA6nnbGIOVCVPaRM+0IEEr5GqPPOskWn2i5Zrn3ATlC42D0jMzv7QnC6ezFCrp3O0i9OH5ra2VequtOZb0kBGgW9kBqvMqDqe7n5dqt7UX4L+mNkNkXjQpTd6nk1EtUpXzXOmDgU1jWUQ9j1uFaiNH6QMJVRsEklCacQl0JoP7DtQsPCVCbBJpWEXtdQGmEqULkggjBVd8u2fQnWz5VHaL/xrWJTw1RMSjCjO6ZTMUJOiLeR31fGtZygyLMaQmKCEqd4f1SNbcNgk0JIor0aQnvDCLGVVQ99hExK8EBowLUaQlaIIpl1Z5z7au8DLIEw9W347shyEs51tSGWK6mOMBU3rPvk2UiUckDLIExPzr8IUYT5k5yqIrRHeE3en5J07/ue5RHaA49xzywLeV7JMFBJhHZ3z7kA3ZwaoJraFKFtf7Gt0GQgN98fpCO0N/+MvBFV0gYVENq9xr9xGL1GoTNhgJDXoCTT2bytA6FaBfQ3dD96Z4bQHrKFHmjUqJycrIrwkEP5u9coK+Z7aAjtnjDbyEAm0bSqiBg6QtteGpYT0EgmWC1LPQjthz3fFu+SJKI9FpKsC2Hqbni/yqqi6RU1F9WB0O70WX3ZYnKSfpk8HS/CVP/Pw9+xcbxwXknHsyFMj+N7Uj9GL/miHUAOhKkB8BUa1frdI+GEX9T6Ix6E6XucRTz5YgxfI5oZvT8WhOl5XEZxHfpRxtHS4PwxIkzpY50wM6sI/DVVP6jEgzBl1pXrGhfEXeB5brAsX41TTFwIU/p8j2IGkMJx41nVmrECYkRo293HeRwFRlvJMnhk7Y4RK8KUOr3lOIzvDEvMQxeH41WPFZ7Nj/BA7Y/Zc+Q2WuU5VrQabjRZPTKITkB1IDxQe9pfu77bdKQonAwppNNM/27dn9aB7kB1Icyo3dutFpMUQNwMHEemYEWGSkjpOEEzTn8zWax2vbrAZVQrwiN1B6PpbrNcbRf78fPEmjyP94vtarnZTUeD6qXElel/pc0XKyYluB8AAAAASUVORK5CYII=" alt=""> <span class="badge bg-blue-600 ms-1">ธนาคารกรุงเทพ</span></label>
                                </div>

                                <div class="collapse multi-collapse" id="multiCollapseExample1">
                                    <div class="form-check mt-3">
                                        <input type="radio" class="form-check-input" name="shop_bank" id="BAY4" value="ธนาคารกรุงศรีอยุธยา" data-parsley-multiple="radiorequired">
                                        <label class="form-check-label h5" for="BAY4"><img width="20" height="20" 
                                        src="https://morningsleep.com/wp-content/uploads/2019/01/morning-sleep-bank-of-ayudhya.jpg" alt=""> <span class="badge bg-warning ms-1">ธนาคารกรุงศรีอยุธยา</span></label>
                                    </div>
                                    <div class="form-check mt-3">
                                        <input type="radio" class="form-check-input" name="shop_bank" id="CIMB5" value="ธนาคารซีไอเอ็มบี" data-parsley-multiple="radiorequired">
                                        <label class="form-check-label h5" for="CIMB5"><img width="20" height="20" 
                                        src="https://play-lh.googleusercontent.com/8H0XgRpvsv7vBB3XzPxCfpsRMaA9x2IkRymC-wU7HDTq9cZsc1qSID7d2Mg2DhjOnYY" alt=""> <span class="badge bg-red-800 ms-1">ธนาคารซีไอเอ็มบี</span></label>
                                    </div>
                                    <div class="form-check mt-3">
                                        <input type="radio" class="form-check-input" name="shop_bank" id="standard6" value="Standard Chartered" data-parsley-multiple="radiorequired">
                                        <label class="form-check-label h5" for="standard6"><img width="20" height="20" 
                                        src="https://yt3.ggpht.com/ytc/AKedOLQlQPfEtY7j7w66GKr2OQF6Z_La4X2k5FegWUgaxA=s900-c-k-c0x00ffffff-no-rj" alt=""> <span class="badge bg-lime ms-1">Standard Chartered</span></label>
                                    </div>
                                    <div class="form-check mt-3">
                                        <input type="radio" class="form-check-input" name="shop_bank" id="ICBC7" value="ธนาคารไอซีบีซี" data-parsley-multiple="radiorequired">
                                        <label class="form-check-label h5" for="ICBC7"><img width="20" height="20" 
                                        src="https://www.bot.or.th/App/drBiz/images/logo/070.png" alt=""> <span class="badge bg-danger ms-1">ธนาคารไอซีบีซี</span></label>
                                    </div>
                                    <div class="form-check mt-3">
                                        <input type="radio" class="form-check-input" name="shop_bank" id="TISCO8" value="ธนาคารทิสโก้" data-parsley-multiple="radiorequired">
                                        <label class="form-check-label h5" for="TISCO8"><img width="20" height="20" 
                                        src="https://seeklogo.com/images/T/tisco-logo-07F1A57983-seeklogo.com.png" alt=""> <span class="badge bg-blue-500 ms-1">ทิสโก้แบงค์</span></label>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button type="button" class="btn btn-link" data-toggle="collapse" href="#multiCollapseExample1" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">แสดงธนาคารทั้งหมด</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row mb-3">
                        <label class="col-lg-3 col-form-label form-label">ชื่อบัญชี <span class="text-danger">*</span> 
                            :</label>
                        <div class="col-lg-5">
                            <input class="form-control" type="text" id="fullname" name="shop_bank_name" placeholder="กรอกชื่อบัญชี" placeholder="Required" data-parsley-required="true">
                        </div>
                    </div>

                    <div class="form-group row mb-3">
                        <label class="col-lg-3 col-form-label form-label">เลขบัญชี <span class="text-danger">*</span> 
                            :</label>
                        <div class="col-lg-5">
                            <input class="form-control" type="text" id="fullname" name="shop_bank_number" placeholder="กรอกเลขบัญชี" placeholder="Required" data-parsley-required="true">
                        </div>
                    </div>

                    <div class="form-group row mb-3">
                        <label class="col-lg-3 col-form-label form-label">สาขา <span class="text-danger">*</span> 
                            :</label>
                        <div class="col-lg-5">
                            <input class="form-control" type="text" id="fullname" name="shop_bank_branch" placeholder="กรอกสาขา" placeholder="Required" data-parsley-required="true">
                        </div>
                    </div>

                    <div class="form-group row mb-3">
                        <label class="col-lg-3 col-form-label form-label">ประเภท <span class="text-danger">*</span> 
                            :</label>
                        <div class="col-lg-5">
                            <select class="form-control" data-field="product_option_stocks][0][stock_id" name="account_type" data-parsley-required="true">
                                <option value="" style="text-align: center">-- เลือกประเภทบัญชี --</option>
                                <option value="ออมทรัพย์">ออมทรัพย์</option>
                                <option value="สะสมทรัพย์">สะสมทรัพย์</option>
                                <option value="กระแสรายวัน">กระแสรายวัน</option>
                                <option value="ฝากประจำ">ฝากประจำ</option>
                                <option value="เผื่อเรียก">เผื่อเรียก</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row mb-3">
                                <label class="col-lg-3 col-form-label form-label" for="description">อัปโหลดรูปภาพ QR
                                    :</label>
                                <div class="col-lg-9">
                                    <!-- BEGIN panel เพิ่มรูปภาพสินค้า -->
                                    <div class="panel panel-inverse" style="border-radius: 15px;">
                                        <label class="col-lg-3 col-form-label form-label"
                                            for="description">เพิ่มรูปภาพ QR
                                        </label>
                                        <div class="offcanvas offcanvas-start" id="offcanvasImgsQrBank"
                                            data-bs-backdrop="false" data-bs-scroll="true">
                                            <div class="offcanvas-header">
                                                <h5 class="offcanvas-title" id="offcanvasLabel">เพิ่มรูปภาพ QR</h5>
                                                <button type="button" class="btn-close text-reset"
                                                    data-bs-dismiss="offcanvas"></button>
                                            </div>

                                        </div>
                                        <!-- BEGIN panel-body -->
                                        <div class="panel-body text-inverse">
                                            <div class="row my-2">
                                                <div class="col col-xl-5 m-0">
                                                    <div class="post-image-collection">
                                                        <p id="listMain"></p>
                                                        <label class="post-image post-image-placeholder empty"
                                                            id="postImgQrBankMain" style="display: block;">
                                                            <input type="file" class="stock-file-upload" id="PhotofileMain"
                                                                name="bank_qr_file[]" required="required"
                                                                accept="image/*" data-order="Main" />
                                                            <span class="icon-camera">
                                                                <i class="fas fa-cloud-upload-alt fa-4x"
                                                                    style="color: #F6F6F6;"></i>
                                                            </span>
                                                            <p class="uppercase" style="color: #1E56A0;">รูปภาพหลัก
                                                                <span style="color: red;">*</span>
                                                            </p>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- END panel เพิ่มรูปภาพสินค้า -->
                                </div>
                            </div>
                    <div class="form-group row">
                        <label class="col-lg-4 col-form-label form-label">&nbsp;</label>
                        <div class="col-lg-8">
                            <button id="" type="submit" class="btn btn-primary w-100px me-5px"><i class="fas fa-lg fa-fw me-10px fa-save"></i>บันทึก</button>
                            <a href="{{ url('/bank/listbank') }}"
                            class="btn btn-default w-100px btn-cancel-bank-create">ยกเลิก</a>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
        <div class="col-xl-2"></div>
    </div>
</div>
@endsection


@section('js')


<!-- ================== tag-it page-js ================== -->
<script src="{{ asset('assets/plugins/tag-it/js/tag-it.min.js') }}"></script>
<!-- <script src="{{ asset('assets/plugins/jquery-migrate/dist/jquery-migrate.min.js') }}"></script> -->
<script src="{{ asset('assets/plugins/parsleyjs/dist/parsley.min.js') }}"></script>
<script src="{{ asset('assets/plugins/sweetalert/dist/sweetalert.min.js') }}"></script>
<script src="{{ asset('assets/plugins/summernote/dist/summernote-lite.min.js') }}"></script>
<!-- ================== tag-it page-js ================== -->
<!-- ================== wysihtml5 page-js ================== -->
<script src="{{ asset('assets/plugins/ckeditor/ckeditor.js') }}"></script>
{{-- <script src="{{ asset('assets/plugins/bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.all.min.js') }}"></script> --}}
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"></script>
<!-- ================== wysihtml5 page-js ================== -->
<!-- ================== dropzone page-js ================== -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.9.3/dropzone.js"></script>
<!-- ================== dropzone page-js ================== -->
<!-- ================== เขียนเอง page-js ================== -->
<script src="{{ asset('assets/js/handdleonscript/products_addpage/data_tables_all.js') }}"></script>
<script src="{{ asset('assets/js/handdleonscript/products_addpage/dinamic_tables.js') }}"></script>
<script src="{{ asset('assets/js/handdleonscript/products_addpage/format_currenct.js') }}"></script>
<script src="{{ asset('assets/js/handdleonscript/products_addpage/select_hide.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/autonumeric/4.1.0/autoNumeric.min.js"></script>
<!-- <script src="{{ asset('assets/js/handdleonscript/products_addpage/img_product_preview.js') }}"></script> -->
<script src="{{ asset('assets/js/handdleonscript/products_addpage/img_preview.js') }}"></script>
<script src="{{ asset('assets/js/handdleonscript/products_addpage/editor.js') }}"></script>
<!-- ================== เขียนเอง page-js ================== -->
<!-- ================== datepicker page-js ================== -->
<script src="{{ asset('assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>
<!-- ================== datepicker page-js ================== -->
<!-- ================== moment page-js ================== -->
<script src="{{ asset('assets/plugins/moment/min/moment.min.js') }}"></script>
<!-- ================== moment page-js ================== -->

@endsection