<?php

namespace App\Http\Controllers;

use Carbon\Carbon; //เกี่ยวกับเวลา
use Illuminate\Http\Request;
use App\Models\ShopBankAccount;
use App\Models\FileUploadImage;

use App\Models\Security\SecurityUserInformationModel;
use Illuminate\Http\UploadedFile;

class ShopBankAccountController extends Controller
{
    public function index()
    {
        $bank_model = new ShopBankAccount();
        $bank = $bank_model->get_data_bank();
        
        // dd($bank); 

        return view('shop_bank_account.index', compact('bank'));

    }

    public function createbank()
    {
        $bank = ShopBankAccount::all();
        return view('shop_bank_account.created', compact('bank'));
    }

    public function store(Request $request)
    {
        $bank = new ShopBankAccount();
        $data_arr = [
            'shop_bank' => $request->shop_bank,
            'shop_bank_name' => $request->shop_bank_name,
            'shop_bank_number' => $request->shop_bank_number,
            'shop_bank_branch' => $request->shop_bank_branch,
            'account_type' => $request->account_type,
        ];
        $add_data_bank = $bank->addbank($data_arr); // เพิ่มรายชื่อสมาชิก
        $get_bank_id = $add_data_bank->shop_bank_account_id;
        
            
        if ($request->hasFile('bank_qr_file')) {
            $qr_bank = $request->file('bank_qr_file');
            $this->bank_qr_file_upload_image($request, $qr_bank, $get_bank_id);
        }
        return redirect()->route('listbank')->with('success','เพิ่มบัญชีธนาคารเสร็จสิ้น');
    }

    public function updatebank(Request $request, ShopBankAccount $ShopBankAccount)
    {
        $shop_bank_account_id = $request->shop_bank_account_id;
        // dd($request->hasFile('bank_qr_file'));
        $request->validate([
            'shop_bank' => 'required',
            'shop_bank_name' => 'required',
            'shop_bank_number' => 'required',
            'shop_bank_branch' => 'required',
            'account_type' => 'required',
        ]);

        $data_arr = [
            'shop_bank' => $request->shop_bank,
            'shop_bank_name' => $request->shop_bank_name,
            'shop_bank_number' => $request->shop_bank_number,
            'shop_bank_branch' => $request->shop_bank_branch,
            'account_type' => $request->account_type,
        ];
        
        if ($request->hasFile('bank_qr_file')) {

            $bank_qr_images = $request->file('bank_qr_file');
            
            $this->bank_qr_file_upload_image($request, $bank_qr_images,$shop_bank_account_id );
        }

        $ShopBankAccount::where('shop_bank_account_id',$shop_bank_account_id)->update($data_arr);
        return redirect()->route('listbank')->with('success','อัปเดตข้อมูลธนาคารเรียบร้อย');
    }

        // หน้าแก้ไขข้อมูลบัญชีธนาคารและเรียกIDรูปภาพมาแสดง
    public function editbank($shop_bank_account_id){
        $bank = ShopBankAccount::find($shop_bank_account_id);
        // dd($bank);
        $file_upload_images = FileUploadImage::select('*')
                            ->where('ref_id', '=' , $shop_bank_account_id)
                            ->where('image_data_table', '=' , 'shop_bank_accounts')
                            ->where('deleted' ,'0')
                            ->first();
        // dd($file_upload_images);
        return view('shop_bank_account.edit', compact('bank', 'file_upload_images','shop_bank_account_id'));
        
    }
        // สถานะเปิดปิดบัญชีธนาคาร
    public function ajaxUpdateStatusBank(Request $request) {

        $bank_model = new ShopBankAccount();

        $role_id = $request->role_id;

        $status = 'success';

        $data_user_role = [
            'is_status' => $request->is_status,
            'updated_at' => Carbon::now(),
        ];
        
        $role_update = $bank_model->updateDataBanks($role_id, $data_user_role); // อัปเดตข้อมูล
    
        if($role_update == 'success') {
            $message = "ทำการอัปเดตข้อมูลกลุ่มสิทธิ์ผู้ใช้งานเสร็จสิ้นค่ะ";
            return response()->json(['status' => $status, 'message' => $message], 200);
        } else {
            $message = "ทำการอัปเดตข้อมูลกลุ่มสิทธิ์ผู้ใช้งานผิดพลาด กรุณาติดต่อแอดมินค่ะ";
            return response()->json(['status' => 'err', 'message' => $message], 200);
        }


    }

        // อัพโหลดรูปภาพ QR
    public function bank_qr_file_upload_image(Request $request, $qr_bank_image, $get_bank_id=null) {
        // dd($get_bank_id);
        foreach ($qr_bank_image as $key => $qr_bank_image) {
            // dd($product_brand_image->getSize());
            $name_gen = hexdec(uniqid());
            // dd($name_gen);
            $img_ext = strtolower($qr_bank_image->getClientOriginalExtension());
            $img_name = $name_gen . '.' . $img_ext;
            // dd($img_name);
            $upload_location = 'file_upload/images/qr_bank/';
            $full_path = $upload_location . $img_name;
            // dd($full_path);
            $original_name = $qr_bank_image->getClientOriginalName();
            $size = $qr_bank_image->getSize();
            $type = $qr_bank_image->getClientOriginalExtension();
            $qr_bank_image->move($upload_location, $img_name);

            FileUploadImage::create([
                'image_name' => $img_name,
                'image_original_name' => $original_name,
                'image_type' => $type,
                'image_size' => $size,
                'image_path' => $full_path,
                'image_data_table' => 'shop_bank_accounts',
                'ref_id' => $get_bank_id,
                'image_order' => $key + 1
            ]);

        }
    }

    public function ajaxDeletedQrcode($image_id = null){
        // dd($image_id);

        $data_arr_list = ['deleted'=> 1];

        $image_model = new FileUploadImage();
       

        $update_img_qrbank = $image_model->updateDataImg($image_id, $data_arr_list);

        if($update_img_qrbank == 'success') {
            $message = 'ทำการอัปเดตข้อมูลบัญชีธนาคารเสร็จสิ้น';
            return response()->json(['status' => $update_img_qrbank, 'message' => $message], 200);
        } else {
            $message = 'ทำการอัปเดตข้อมูลบัญชีธนาคารผิดพลาด';
            return response()->json(['status' => $update_img_qrbank, 'message' => $message], 200);
        }
    }
}