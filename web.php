<?php

use App\Http\Controllers\StockController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProductBrandController;

use Illuminate\Support\Facades\Route;

// controller
use App\Http\Controllers\manageSetting\manageUnit\ManageUnitController;
use App\Http\Controllers\product\mainCategory\CategoryController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\PermissionGroupController;
use App\Http\Controllers\TransportsController;
use App\Http\Controllers\BillsController;

use App\Http\Controllers\authentication\AuthController;
use App\Http\Controllers\authentication\UserController;
use App\Http\Controllers\dashboard\DashboardController;
use App\Http\Controllers\dashboard\SeoController;
use App\Http\Controllers\member\MemberController;

use App\Http\Controllers\product\mainProduct\ProductPromotionController;

use App\Http\Controllers\gallery\GalleryController;

use App\Http\Controllers\article\ArticleController;
use App\Http\Controllers\api\MediaApiController;

use App\Http\Controllers\payment_notices\PaymentNoticesController;
use App\Http\Controllers\order\OrderController;
use App\Http\Controllers\mail\EmailController;

use App\Http\Controllers\ShopBankAccountController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [AuthController::class, 'index']);
Route::post('/', [AuthController::class, 'signIn'])->middleware('throttle:/')->name('login');
Route::get('/logout', [AuthController::class, 'signOut'])->name('signout');


Route::prefix('dashboard')->middleware('admin.auth')->group(function () {
    Route::get('', [DashboardController::class, 'index'])->name('index-dashboard');
    Route::get('/user-update-auto', [UserController::class, 'autoUpdateStatusIsChangeUser'])->name('auto-update-password');
});


Route::prefix('products')->middleware('admin.auth')->group(function () {
    Route::get('/editproduct/{product_id}/{content_active_id}', [ProductController::class, 'edit'])->name('editproduct');
    Route::get('', [ProductController::class, 'index'])->name('products');
    Route::get('/addproduct', [ProductController::class, 'create'])->name('addproduct');
    Route::post('/storeproduct', [ProductController::class, 'store'])->name('storeproduct');
    Route::post('/update_product_data', [ProductController::class, 'update_product_data'])->name('update_product_data');
    Route::post('/add_product_option', [ProductController::class, 'add_product_option'])->name('add_product_option');
    Route::post('/edit_product_option', [ProductController::class, 'edit_product_option'])->name('edit_product_option');
    Route::post('/delete_product_option', [ProductController::class, 'delete_product_option'])->name('delete_product_option');
    Route::post('/delete_product_image', [ProductController::class, 'delete_product_image'])->name('delete_product_image');
    Route::post('/delete_current_product_option', [ProductController::class, 'delete_current_product_option'])->name('delete_current_product_option');
    Route::post('/delete_current_product_option_stock', [ProductController::class, 'delete_current_product_option_stock'])->name('delete_current_product_option_stock');
    Route::post('/update_product_option', [ProductController::class, 'update_product_option'])->name('update_product_option');
    Route::get('/product-move-trash/{id}/{segment}', [ProductController::class, 'getProductIDMoveTrush'])->name('product_move_trush');
    Route::get('/index-price', [ProductController::class, 'indexProductPricePage'])->name('price-index');
    Route::get('/add-price', [ProductController::class, 'createProductPricePage'])->name('create-product-price');
    Route::post('/index-price/update-price-follow-member-group/{option_id}', [ProductController::class, 'updateProductPriceOfMemberGroup']);
    Route::get('/index-setting-product', [ProductController::class, 'settingProduct'])->name('index-setting');
    Route::post('/index-setting-product/update-setting-product', [ProductController::class, 'updateSettingProduct'])->name('update-setting-product');
    Route::get('/image/{img}', [ProductController::class, 'getShowImgProducts']);
    Route::get('/get-check-sku', [ProductController::class, 'getAjaxCheckSKU']);
});


Route::prefix('seo')->middleware('admin.auth')->group(function () {
    Route::get('', [SeoController::class, 'index']);
});

// Route::resource('stocks', StockController::class)->middleware(['admin.auth', 'check.permission']);
Route::resource('stocks', StockController::class)->middleware(['admin.auth']);
Route::get('changeStatus', [StockController::class, 'delete'])->middleware(['admin.auth']);
Route::post('search_stock', [StockController::class, 'search'])->middleware(['admin.auth']);
Route::post('stock_ajax_store', [StockController::class, 'ajax_store'])->middleware(['admin.auth']);


Route::resource('product_brands', ProductBrandController::class)->middleware(['admin.auth']);
Route::post('product_brands/ajax_store', [ProductBrandController::class, 'ajax_store'])->middleware(['admin.auth']);
Route::post('product_brands/ajax_update', [ProductBrandController::class, 'ajax_update'])->middleware(['admin.auth']);
Route::post('product_brands/ajax_delete', [ProductBrandController::class, 'ajax_delete'])->middleware(['admin.auth']);
  

Route::prefix('product-brands')->middleware('admin.auth')->group(function () {
    Route::get('/index-product-brands', [ProductBrandController::class, 'indexProductBrand'])->name('index-pruduct-brand');
    Route::get('/add-product-brands', [ProductBrandController::class, 'addProductBrandPage'])->name('add-pruduct-brand');
    Route::get('/edit-product-brands/{brand_id}', [ProductBrandController::class, 'editProductBrandPage'])->name('edit-pruduct-brand');
    Route::post('/edit-product-brands/updated-product-brand', [ProductBrandController::class, 'updatedProductBrand'])->name('update-pruduct-brand');
    Route::get('/move-image-trash', [ProductBrandController::class, 'ajaxMoveImageBrandTrash'])->name('move-trash-img-pruduct-brand');
    Route::get('/image/{img}', [ProductBrandController::class, 'getShowImgProductBrand']);
});

Route::prefix('category')->middleware('admin.auth')->group(function () {
    Route::get('', [CategoryController::class, 'indexCategory'])->name('index-category');
    Route::get('/image/{img}', [CategoryController::class, 'getShowImgCategory']); // เรียกรูปภาพ
    Route::get('/data-show-root-tree', [CategoryController::class, 'getDataCategoryAjaxTreeArray']);
    Route::get('/add-category', [CategoryController::class, 'addCategoryPage']);
    Route::get('/add-category/data-node-category', [CategoryController::class, 'getDataIDCategory']);  // ajax ไปเรียกให้ข้อมูลเรียงกัน
    Route::post('/add-category/add-data-category', [CategoryController::class, 'addDataCategory'])->name('add-data-category');
    Route::get('/edit-category/{num}', [CategoryController::class, 'editPageCategory'])->name('edit-category');
    Route::get('/edit-category/image-category/{num}/{segment}', [CategoryController::class, 'editImageCategory']); // ใช้ ajax ไปอัปเดตข้อมูล deleted = 1 และข้อมูล image_id ใน category ให้เป็น null
    Route::post('/edit-category/image-category/', [CategoryController::class, 'updateCategory']);
    Route::get('/ajax_status', [CategoryController::class, 'ajaxChangeStatus']);
});


Route::resource('permissions', PermissionController::class)->middleware('admin.auth');

Route::resource('menus', MenuController::class)->middleware('admin.auth');

Route::resource('permission_groups', PermissionGroupController::class)->middleware('admin.auth');

Route::prefix('/setting/manage-main-data/unit')->middleware('admin.auth')->group(function () {
    Route::get('', [ManageUnitController::class, 'indexUnit'])->name('index-unit');
    Route::post('addDataunit', [ManageUnitController::class, 'addUnit'])->name('add-unit');
    Route::post('updateDataunit', [ManageUnitController::class, 'updateUnit'])->name('update-unit');
    Route::get('updateMoveStatusDataUnit', [ManageUnitController::class, 'updateMoveStatusUnitTrash']);
});

Route::prefix('/setting/manage-main-data/user')->middleware('admin.auth')->group(function () {
    Route::post('/user-information-update', [UserController::class, 'updatedUserInformation'])->name('user-information-updated');
    Route::post('/user-security-update', [UserController::class, 'updatePasswordUser'])->name('user-security-updated');
    Route::get('/list-users', [UserController::class, 'getListUsers'])->name('users-list');
    Route::post('/list-users/add-user', [UserController::class, 'addListUsers'])->name('add-users-list');
    Route::post('/list-users/update-user', [UserController::class, 'updatedListUsers'])->name('updated-users-list');
    Route::get('/blacklist-anonymous-user', [UserController::class, 'getBlacklistUser'])->name('get-users-blacklist');
    Route::get('/setting-user', [UserController::class, 'getSettingUser'])->name('get-user-setting');
    Route::post('/setting-user/update-setting-user', [UserController::class, 'updateSettingUser'])->name('update-user-setting');
    Route::get('/permission_groups/ajax-update-status', [PermissionGroupController::class, 'ajaxUpdateStatusPermissionGroups'])->name('ajax-update-permission-groups');
    Route::get('/permission_groups/delete-permission-groups/{permission_groups_id}', [PermissionGroupController::class, 'deleteStatusPermissionGroups'])->name('delete-permission-groups');
});

Route::get("/permissiondenined", function () {
    return view("error.permission");
})->middleware('admin.auth');

Route::get("/store", function () {
    return view("store.create");
})->middleware('admin.auth');

Route::get("/banks", function () {
    return view("payment.bank.list");
})->middleware('admin.auth')->name('listbank');

Route::get("/banks/create", function () {
    return view("payment.bank.create");
})->middleware('admin.auth')->name('createbank');

Route::get("/credit_cards", function () {
    return view("payment.credit_card.list");
})->middleware('admin.auth')->name('listcreditcard');

Route::get("/credit_cards/create", function () {
    return view("payment.credit_card.create");
})->middleware('admin.auth')->name('createcreditcard');

// Route::get("/transport/create", function () {
//     return view("transport.create");
// })->name('createtransport');

Route::prefix('transport')->middleware('admin.auth')->group(function () {
    Route::get('', [TransportsController::class, 'index'])->name('index_transport_page');
    Route::get('/create', [TransportsController::class, 'create'])->name('create_transport_page');
});

Route::prefix('delivery')->middleware('admin.auth')->group(function () {
    Route::get('', [TransportsController::class, 'indexDeliveryPage'])->name('index_delivery_page');
    Route::post('/create', [TransportsController::class, 'createDelivery']);
    Route::get('/ajax-update-status', [TransportsController::class, 'ajaxUpdateSwicthStatus']);
    Route::post('/update', [TransportsController::class, 'updateDelivery']);

});

// Route::post('/transport/store', [TransportsController::class, 'store'])->name('storetransport');

Route::prefix('check_slip')->middleware('admin.auth')->group(function () {
    Route::get('', [PaymentNoticesController::class, 'indexPaymentNoticesPage'])->name('check_slip');
    Route::get('/image/{img_name}', [PaymentNoticesController::class, 'getShowImgCheckSlip']);
    Route::post('/update-slip-confirm', [PaymentNoticesController::class, 'updatedCheckSlip']);
    Route::get('/ajax-update-slip-confirm', [PaymentNoticesController::class, 'ajaxUpdatedCheckSlip']);
});


Route::get("/bill", [BillsController::class, 'list'])->middleware('admin.auth')->name('list_bill');
Route::get("/bill/create", [BillsController::class, 'create'])->middleware('admin.auth')->name('create_bill');
Route::post("/bill/store", [BillsController::class, 'store'])->middleware('admin.auth')->name('bills.store');


Route::prefix('/member')->middleware('admin.auth')->group(function(){
    Route::get('/member-group', [MemberController::class, 'indexMemberGroup'])->name('member-group');
    Route::post('/member-group/add-member-group', [MemberController::class, 'addMemberGroup'])->name('add-member-group');
    Route::post('/member-group/update-member-group', [MemberController::class, 'updateMemberGroup']);
    Route::get('/member-group/delete-member-group/{num}', [MemberController::class, 'destroyMemberGroup'])->name('delete-member-group');
    Route::get('/list-member', [MemberController::class, 'indexListMember'])->name('list-member');
    Route::post('/list-member/add-list-member/added-list-member', [MemberController::class, 'addListMember'])->name('add-list-member');
    Route::get('/list-member/add-list-member', [MemberController::class, 'addListMemberPage'])->name('add-list-member-page');
    Route::post('/list-member/update-group-list-member', [MemberController::class, 'updateGropuListMember'])->name('update-group-list-member');
    Route::get('/list-member/update-status-list-member', [MemberController::class, 'updateStatusListMember'])->name('update-status-list-member');
    Route::get('/list-member/update-list-member/{num}', [MemberController::class, 'updateListMemberPage'])->name('list-member/update-list-member/{num}');
    Route::post('/list-member/update-list-member/updated-list-member', [MemberController::class, 'updateListMember'])->name('updated-list-member');
    Route::get('/list-member/add-shipping-list-member/{num}', [MemberController::class, 'addShippingMemberPage'])->name('list-member/add-shipping-list-member/{num}');
    Route::get('/list-member/address-province', [MemberController::class, 'ajaxRequestProvince'])->name('address-province');
    // Route::get('/list-member/address-armphur', [MemberController::class, 'ajaxRequestDistrict']);
    Route::post('/list-member/add-shipping-list-member/add-shipping-member', [MemberController::class, 'addedShippingMember'])->name('added-shipping-member');
    Route::get('/list-member/update-shipping-list-member/{num}/{id}', [MemberController::class, 'updateShippingMemberPage'])->name('update-shipping-list-member');
    Route::post('/list-member/update-shipping-list-member/update-shipping-of-member}', [MemberController::class, 'updateShippingMember'])->name('updated-shipping-member');
    Route::get('/list-member/deleted-shipping-member/{id}', [MemberController::class, 'destroyShippingMember']);
    Route::get('/list-member/add-bill-tax-member/{num}', [MemberController::class, 'addBillTaxMemberPage'])->name('list-member/add-bill-tax-member/{num}');
    Route::post('/list-member/add-bill-tax-member/added-bill-tax-for-member', [MemberController::class, 'addedBillTaxMember'])->name('added-bill-tax-member');
    Route::get('/list-member/update-bill-tax-member/{num}/{id}', [MemberController::class, 'updateBillTaxMemberPage'])->name('list-member/update-bill-tax-member/{num}/{id}');
    Route::post('/list-member/update-bill-tax-member/updated-bill-tax-member', [MemberController::class, 'updateBillTaxMember'])->name('updated-bill-tax-member');
    Route::get('/list-member/deleted-bill-tax-member/{id}', [MemberController::class, 'destroyBillTaxMember']);
    Route::get('/list-member/ajax-shipping-is-default', [MemberController::class, 'ajaxUpdateIsDefualtShipping']);
    Route::get('/list-member/ajax-tax-is-default', [MemberController::class, 'ajaxUpdateIsDefualtTax']);
    Route::post('/list-member/ajax-uploaded-img-member/{id}/', [MemberController::class, 'ajaxUpdateImageMember']);
    Route::get('/image/{img}', [MemberController::class, 'getShowImgProfileMember']); // เรียกรูปภาพ
    Route::get('/list-member/ajax-delete-image', [MemberController::class, 'ajaxDestroyImageMember']);
    Route::post('/list-member/update-data-system-member', [MemberController::class, 'updatedSystemMember'])->name('update-data-system-member');
    Route::get('/member-setting', [MemberController::class, 'memberSettingPage'])->name('member-setting');
    Route::get('/list-member/update-chage-is-delete/{num}/{typename}', [MemberController::class, 'updateMemberSettingChangeStatusIsDelete']);
    Route::get('/list-member/destroy-status-is-delete/{num}/{typename}', [MemberController::class, 'destroyMemberSettingStatusIsDelete']);
    Route::post('/member-setting/update-setting-member', [MemberController::class, 'updateMemberSetting'])->name('update-setting-member');
});
    // MemberController  

Route::prefix('/gallery')->middleware('admin.auth')->group(function(){
    Route::get('/gallery-index', [GalleryController::class, 'indexGallery'])->name('gallery-index');
    Route::get('/gallery-index/destroy-image', [GalleryController::class, 'ajaxDestroyGallery']);
    Route::get('/image-product-popup', [GalleryController::class, 'indexImageProductPopUpIndexPage'])->name('index-image-product-popup-page');
    Route::post('/image-product-popup/uploads', [GalleryController::class, 'uploadImageProductPopUp']);
    Route::post('/image-product-popup/re-order', [GalleryController::class, 'ajaxReOrderImageProductPopUp']);
    Route::get('/trash-image/{popup_id}/{uploads_id}/{user_id}', [GalleryController::class, 'deletedImageProductPopUp']);
    Route::post('/update-image-popup', [GalleryController::class, 'ajaxUpdateStatusImageProductPopUp']);
    Route::post('/image-product-popup/edit-uploads', [GalleryController::class, 'editUploadImageProductPopUp']);
});

Route::prefix('/article')->middleware('admin.auth')->group(function(){
    Route::get('/article-index', [ArticleController::class, 'indexArticle'])->name('article-index');
    Route::get('/add-article', [ArticleController::class, 'addedArticlePage'])->name('article-added-page');
    Route::get('/category-article-index', [ArticleController::class, 'indexCategoryArticlePage'])->name('category-article-index');
    Route::post('/add-category-article', [ArticleController::class, 'addCategoryArticle'])->name('category-article-added');
    Route::post('/updated-category-article', [ArticleController::class, 'updatedCategoryArticle'])->name('category-article-updated');
    Route::get('/deleted-category-article/{num}', [ArticleController::class, 'destroyCategoryArticle']);
    Route::post('/add-article/create-article', [ArticleController::class, 'addedArticle'])->name('create-article');
    Route::get('/image/{img}', [ArticleController::class, 'getShowImg'])->name('image'); // เรียกรูปภาพ
    Route::get('/edit-article/{num}', [ArticleController::class, 'editArticlePage'])->name('edit-article');
    Route::get('/index-article/chage-is-status', [ArticleController::class, 'ajaxChangIsStatus']);
    Route::get('/edit-article/remove-image-article/{id}/{num}', [ArticleController::class, 'ajaxRemoveImageArticleBlog']);
    Route::post('/edit-article/update-article', [ArticleController::class, 'editArticleUpdated'])->name('edit-article/update-article');
    Route::get('/deleted-article-blog/{blog_id}/{img_id}/{seo_id}', [ArticleController::class, 'destroyArticleBlog'])->name('deleted-article-blog/{blog_id}/{img_id}/{seo_id}');
    Route::get('/setting-policy', [ArticleController::class, 'settingArticleBlog'])->name('setting-article');
    Route::get('/setting-policy/add-policy', [ArticleController::class, 'settingArticlePolicyAddPage'])->name('setting-article-add-policy');
    Route::post('/setting-policy/add-policy/create-policy', [ArticleController::class, 'settingCreateArticlePolicy']);
    Route::get('/setting-policy/edit-policy/{policy_id}', [ArticleController::class, 'settingEditArticlePolicyPage'])->name('setting-policy/edit-policy/{policy_id}');
    Route::get('/setting-policy/chage-is-draf', [ArticleController::class, 'ajaxUpdateSettingArticlePolicy'])->name('setting-policy/chage-is-draf');
    Route::post('/setting-policy/edit-policy/update-policy', [ArticleController::class, 'settingUpdateArticlePolicy']);
    Route::get('/setting-policy/deleted-policy/{policy_id}/{seo_id}', [ArticleController::class, 'deletedDataSettingPolicy'])->name('setting-policy/deleted-policy/{policy_id}/{seo_id}');
});

Route::prefix('media')->middleware('admin.auth')->group(function () {
    Route::get('/index', [MediaApiController::class, 'indexMediaPage'])->name('index_media_page');
    Route::get('/add-media', [MediaApiController::class, 'addedMediaPage'])->name('added_media_page');
});


Route::prefix('order')->middleware('admin.auth')->group(function () {
    Route::get('/', [OrderController::class, 'indexOrderPage'])->name('index_order_page');
    Route::get('/create-order', [OrderController::class, 'createOrderPage'])->name('create_order_page');
    Route::get('/create-order/{member_group_id}/{order_id}', [OrderController::class, 'getDataOrderPriceMemberGroup'])->name('/create-order/{member_group_id}/{order_id}');
    Route::get('/find-data-address-member', [OrderController::class, 'ajaxGetDataMember']);
    Route::get('/find-data-tax-address-member', [OrderController::class, 'ajaxGetDataTaxAddressMember']);
    Route::get('/order-status/{order_id}/{status}', [OrderController::class, 'orderStatus'])->name('order-status/{order_id}/{status}');
    Route::post('/update-tracking-description/{order_id}/{status}/{segment}', [OrderController::class, 'orderUpdateStatusTrackingDescription']);
    Route::get('/setting-order', [OrderController::class, 'settingOrderPage'])->name('setting-order');
    Route::post('/setting-order/update-setting-order', [OrderController::class, 'updateSettingOrder'])->name('update-setting-order');
    Route::get('/order_receipt/pdf/{order_number}', [OrderController::class, 'orderReceiptPDFPage']); // ใบเสร็จรับเงิน
    Route::get('/order_delivery/pdf/{order_number}', [OrderController::class, 'orderDeliveryPDFPage']); // ใบเสร็จรับเงิน
    Route::get('/order_requisition/pdf/{order_number}', [OrderController::class, 'orderRequisitionPDFPage']); // ใบเบิกสินค้า
    Route::get('/order_parcel_label/pdf/{order_number}', [OrderController::class, 'orderParcelLabelPDFPage']); // จ่ายหน้ากล่องสินค้า
    Route::get('/product-preparation', [OrderController::class, 'orderProductPreparationPage'])->name('order_product_preparation'); 
    Route::post('/update-order-description', [OrderController::class, 'orderUpdateStatusDescription']);
    Route::post('/update-order-status', [OrderController::class, 'orderUpdateStatus']);
    Route::post('/update-change-status-product-preparation', [OrderController::class, 'orderUpdateStatusProductPreparation']); // อัปเดพตสถานะเป็นเตรียมจัดส่ง
});


Route::get('send-email', [EmailController::class, 'statusEmailOrder'])->middleware(['admin.auth']);

Route::get('send-mail', function () {
   
    $details = [
        'title' => 'Mail from ItSolutionStuff.com',
        'body' => 'This is for testing email using smtp'
    ];
   
    \Mail::to('akenoppawut@gmail.com')->send(new \App\Mail\AttisiaMail($details));
   
    dd("Email is Sent.");
});



Route::get("/bank/listbank",[ShopBankAccountController::class, 'index'])->middleware('admin.auth')->name('listbank');

Route::get("/bank/create", [ShopBankAccountController::class, 'createbank'])->middleware('admin.auth')->name('bankcreate');

Route::post("/bank/listbank", [ShopBankAccountController::class, 'store'])->middleware('admin.auth')->name('listbank2');

Route::get('/bank/edit/{shop_bank_account_id}', [ShopBankAccountController::class, 'editbank'])->middleware('admin.auth')->name('editbank');

Route::put('/bank-update/{shop_bank_account_id}', [ShopBankAccountController::class,'updatebank'])->middleware('admin.auth')->name('updatebank');

Route::get('/bank/ajax-update-status', [ShopBankAccountController::class, 'ajaxUpdateStatusBank'])->name('ajax-update-bank');

Route::get('/bank/ajax-deleted-qrcode/{image_id}', [ShopBankAccountController::class, 'ajaxDeletedQrcode'])->name('ajax-update-bank');

