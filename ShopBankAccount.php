<?php

namespace App\Models;
use App\Models\FileUploadImage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShopBankAccount extends Model
{
    use HasFactory;
    protected $table = 'shop_bank_accounts';
    protected $primaryKey ='shop_bank_account_id';
    protected $fillable = [
        'shop_bank',
        'shop_bank_name',
        'shop_bank_number',
        'shop_bank_branch',
        'account_type',
        'is_status',
        'create_at',
        'update_at',
        'deleted',
    ];

    public function getDataBankById($shop_bank_account_id) {
        $shop_bank_accounts = ShopBankAccount::find($shop_bank_account_id)->toArray();
        return $shop_bank_accounts;
    }

    public function addbank($data_arr) { // เพิ่มรายชื่อสมาชิก
        $tbl_bank = ShopBankAccount::create($data_arr);
        return $tbl_bank;
    }

    public function get_data_bank() {
        $tbl_bank = ShopBankAccount::with('bank_qrcode_file_image')->get();
        return $tbl_bank;
    }

    public function qr_bank_file_image() {
        return $this->hasMany(FileUploadImage::class, 'ref_id', 'stock_id');
    }


    public function bank_qrcode_file_image()
    {
    return $this->hasMany(FileUploadImage::class, 'ref_id', 'shop_bank_account_id')->where('file_upload_images.image_data_table', 'shop_bank_accounts')->where('deleted',0);
    }
    
    public function updateDataBanks($shop_bank_account_id, $data_arr) {

        $tbl_bank = ShopBankAccount::where('shop_bank_account_id', $shop_bank_account_id);
        $tbl_bank->update($data_arr);
  
        return 'success';
      }

      
    
}
